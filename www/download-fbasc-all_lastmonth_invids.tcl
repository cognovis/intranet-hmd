# /package/intranet-hmd/download-fbasc-all_variable.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Download the HMD FBASC CSV File for all companies

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @author etm
    @creation-date November 2016
} {
    {return_url ""}
    {month ""}
}

set user_id [ad_maybe_redirect_for_registration]

        db_1row todays_date "
		select
			to_char(sysdate::date, 'YYYY') as todays_year,
			to_char(sysdate::date, 'MM') as todays_month,
			to_char(sysdate::date, 'DD') as todays_day
		from dual
		"

set last_year [db_string  ly "select to_char(current_date - interval '1 year', 'YYYY')"]
set last_month [db_string  lm "select to_char(current_date - interval '1 month', 'YYYYMM')"]
if {"" == $month} {set month  $last_month}
set next_month [expr $month + 1]
if {$next_month == 13} {set next_month 01}
if {[string length $next_month] == 1 } {set  next_month "0$next_month"}
set next_month "$todays_year$next_month"


ns_log Notice "month: $month"
ns_log Notice "last_month: $last_month"
ns_log Notice "next_month: $next_month"
# ---------------------------------------------------------------
# Customer invoices
# ---------------------------------------------------------------

set interco_options [db_list_of_lists interco "	select c.company_name, c.company_id 
										from im_companies c 
										where c.company_status_id in (select * from im_sub_categories(46)) 
											and c.company_type_id = 11000000 
										order by lower(c.company_name)"]

set zip_dir [ns_tmpnam]
file mkdir $zip_dir
ns_log Notice "zip-dir is $zip_dir"

set file_created_p 0
set failed_invoice_ids [list]

foreach interco_option $interco_options {
    set company_id [lindex $interco_option 1]
    set company_name [lindex $interco_option 0]
    

    foreach cost_type_id [list [im_cost_type_invoice] [im_cost_type_correction_invoice]] {
        # ---------------------------------------------------------------
        # Get the list of invoices created and effective_date within same month
        # ---------------------------------------------------------------
        
        db_1row todays_date "
		select
			to_char(sysdate::date, 'YYYY') as todays_year,
			to_char(sysdate::date, 'MM') as todays_month,
			to_char(sysdate::date, 'DD') as todays_day
		from dual
		"
	
        set firstofmonth "$todays_year-$todays_month-01"
	   set today [db_string get_today_date "select sysdate from dual"]
        
        set invoice_ids [db_list export_invoices "select c.cost_id 
						  from im_costs c,
							  im_projects p,
							  im_invoices i
						  where cost_type_id = :cost_type_id 
							  and p.project_id = c.project_id 
							  and p.interco_company_id = :company_id 
							  and i.invoice_id = c.cost_id 
							  and i.invoice_id in ('3277555', '3277536', '3277558', '3277551', '3790431', '3790975', '3792074', '3314025', '3314737', '3316876', '3317294', '3317114', '3317795', '3318515', '3318703', '3318695', '3317886', '3319131', '3319089', '3317548', '3794019', '3794664', '3765715', '3221825', '3222013', '3231279', '3231303', '4144733', '4101936', '4102354', '4104718', '4105378', '4105649', '4105992', '4106401', '4106671', '4106831', '4106938', '4107512', '4107530', '4108052', '4108651', '4108775', '4104879', '4109739', '4109850', '4110017', '4107910', '4111415', '4111459', '4112605', '4112719', '4112826', '4110879', '4107066', '4114016', '4113989', '4110361', '4110138', '4115087', '4115096', '4115763', '4108878', '4115883', '4116750', '4117140', '4117153', '4117196', '4117577', '4109058', '4104961', '4119542', '4119894', '4119126', '4121068', '4121181', '4121279', '4121920', '4114150', '4122661', '4122781', '4113156', '4123036', '4123491', '4123651', '4123770', '4124297', '4124580', '4124648', '4124795', '4124799', '4125309', '4125700', '4126397', '4127457', '4128030', '4128815', '4129529', '4123373', '4131139', '4131235', '4131610', '4132117', '4133146', '4133415', '4133805', '4126144', '4133963', '4134445', '4129606', '4132357', '4134704', '4134770', '4135152', '4135678', '4136251', '4136396', '4136424', '4136955', '4137000', '4103222', '4104433', '4094730', '4082695', '4103636', '4104445', '4092233', '4102878', '4103624', '4097460', '4100690', '4104254', '4138031', '4138095', '4139370', '4139811', '4141205', '4141546', '4141615', '4141881', '4142123', '4142151', '4143470', '4135853', '4143987', '4144519', '4113490', '4145364', '4145645', '4141096', '4146590', '4146607', '4141748', '4137898', '4148798', '4129346', '4113245', '4129060', '4129734', '4136523', '4142459', '4143841', '4110247', '4116337', '4123866', '4129230', '4140718', '4141009', '4145053', '3316736', '4109727', '4121065', '4129973', '4132559', '4105223', '4106965', '4111739', '4122785', '4134662', '4139849', '4142663', '4142767', '4109518', '4115694', '4124926', '4125295', '4125524', '4102815', '4119639', '4119940', '4124595', '4129985', '4144399', '3315657', '4104890', '4123030', '4123545', '4128286', '3318769', '4113707', '4122527', '4126129', '4142465', '4138105', '4139394', '4140109', '4145689', '4106887', '4108790', '4115179', '4122413', '4143378', '4109646', '4111186', '4129569', '4141454', '4106595', '4118500', '4118946', '4130876', '4141667', '4108084', '4119159', '4138735', '4141627', '4143276', '3315857', '3794074', '3794035', '4117434', '4122511', '4136254', '3316981', '4108797', '4111772', '4112450', '4113207', '4120110', '4125997', '4126354', '4125397', '4141132', '3318431', '4111889', '4117651', '4132294', '4136367', '4137643', '4143491', '4107635', '4125139', '4134246', '4126853', '4134100', '4098224', '4123417', '4123816', '4129959', '4140085', '3791323', '4106828', '4107996', '4113899', '4114140', '4119512', '4123785', '4138629', '4116682', '4130843', '4132300', '3791588', '3316951', '3759376', '4116251', '4115118', '4128055', '4099347', '4104393', '4107515', '4131814', '4099637', '4138714', '4107546', '4138662', '4107419', '4109160', '4114032', '4114174', '4118445', '4124833', '4132831', '4134228', '4143412', '4106903', '4139526', '4143476', '4148770', '4137636', '4118644', '4119587', '4124842', '4133206', '4138514', '3313551', '4105342', '4113963', '4129284', '4130798', '4131311', '3317243', '4108722', '4123456', '4133787', '4145904', '4108049', '4112055', '4114788', '4121940', '4132308', '4119123', '4119598', '4119856', '4124901', '4125419', '4132110', '4136383', '4138655', '4116711', '4128136', '4134198', '4098145', '4141322', '4146024', '4114575', '4121738', '4100202', '4148794', '3318834', '4118477', '4130416', '4139600', '4107129', '4117396', '4121383', '4124864', '4136343', '4110007', '4117211', '4144141', '4111575', '4124434', '4142701', '3317075', '4110606', '4127895', '4131888', '4134287', '4135191', '4141328', '3271460', '3314031', '4107986', '4105366', '4111068', '4113134', '4113870', '4116881', '4119506', '4120083', '4121899', '4130398', '4131574', '4066358', '4139616', '4111852', '4113667', '4125401', '4130205', '4132068', '4133979', '4138721', '4113435', '4119668', '4131558', '4137691', '4142347', '4124728', '4135962', '4100282', '4140040', '4119291', '4127649', '4136656', '4107472', '4119781', '4126847', '4131453', '4135251', '4104386', '4148179', '3793383', '4118435', '4119144', '4121291', '4122283', '3791502', '4133878', '3317098', '4115236', '4119853', '4136872', '4145158', '3791596', '4105706', '4109759', '4113385', '4116776', '4118442', '4119771', '4123366', '4095272', '4138616', '4140205', '4145536', '4117640', '4111879', '4128955', '4138638', '4139719', '4143464', '4148585', '3792364', '4117814', '4142653', '4143432', '4125302', '4115726', '4127693', '4129873', '4134117', '4135212', '4138682', '4138846', '4139374', '3791844', '4121707', '4135209', '4095024', '4125316', '4131413', '4147836', '4107523', '4112006', '4131555', '4134189', '4134238', '4141565', '4116912', '4121250', '4131604', '4141555', '4142508', '4148450', '4112038', '4113090', '4114337', '4123679', '4136142', '4102808', '4103219', '4125786', '4141198', '4107626', '4108201', '4112422', '4115186', '4122292', '4122999', '4136892', '4139723', '4147084', '4147777', '3315568', '4119778', '4146628', '3307058', '4132080', '4134491', '4141601', '4115474', '4117839', '4125870', '4136563', '4137332', '4122693', '4129078', '4137719', '4113966', '3314925', '4110121', '4119957', '4128502', '4116726', '4120313', '4124854', '4127833', '4131908', '4098099', '3793042', '4119810', '4124916', '4130816', '4135740', '4109199', '4109721', '4115263', '4134669', '4115707', '4111235', '4117600', '4120202', '4129610', '4119875', '4128113', '4131436', '4134697', '4137921', '4114166', '4115461', '4123889', '4128674', '4132514', '4105607', '4112052', '4113715', '4117040', '4122340', '4123299', '4125716', '4128509', '4146745', '3315817', '3794703', '4106186', '4120057', '4139434', '3314961', '4105236', '4110020', '3792978', '3794104', '4120953', '4130828', '4103251', '4135617', '4121099', '4133970', '4102150', '3791081', '3316475', '3794189', '4130223', '4133510', '4112615', '4120821', '4134268', '4136285', '4136544', '4100336', '4102178', '4139972', '4144079', '4129481', '3315911', '3318050', '4120628', '4123296', '4130910', '4100242', '4143282', '4114269', '3316900', '4111951', '4117957', '4129057', '4120322', '4131791', '4108892', '4109688', '4115524', '4118530', '4097945', '4141109', '4145993', '3787961', '4113983', '4129361', '4140074', '4143882', '4144580', '4105384', '4114470', '4115714', '4123883', '4139855', '4125239', '4132074', '4146473', '4119929', '4115768', '4123858', '4126991', '4136334', '4141441', '3794700', '4117735', '4139144', '4146360', '4115677', '4115260', '4126302', '4134258', '4136946', '4102868', '4145761', '4106410', '4107071', '4112348', '4131797', '4126016', '4141714', '4141773', '4107656', '4107983', '4116667', '4121780', '4133681', '4136541', '4137649', '4097985', '4141988', '3748937', '3780895', '4122299', '4124775', '4138363', '4108192', '4115688', '4125038', '4126067', '4126859', '4139537', '4142206', '4108755', '4109052', '4114128', '4124343', '4133739', '4098730', '3794747', '4117202', '4125057', '4125766', '4140092', '4141821', '4111373', '4137608', '4102088', '4098747', '4112031', '4118600', '4127504', '4130174', '4132819', '4138298', '4139843', '4104883', '4106098', '4108886', '4114481', '4116812', '4142435', '4147638', '3318603', '4106010', '4110109', '4109148', '4123073', '4129835', '4134252', '4136949', '4142613', '4123813', '4126126', '4132567', '4134707', '4117722', '4125750', '4130822', '4132020', '4137685', '4141973', '4142451', '4108613', '4115308', '4124229', '4139799', '3313442', '4110553', '4097024', '4146427', '4118608', '4117920', '4133378', '4147666', '4117703', '4122825', '4133755', '4147682', '3790411', '4119297', '4126290', '4142344', '4114041', '4109176', '4114588', '4136808', '4105448', '4110440', '4115111', '4119545', '4120916', '4124577', '4138111', '4139437', '4147759', '4152952', '3774358', '4110542', '4111539', '4120209', '4128874', '4098893', '4120229', '4122105', '4144342', '3794286', '4112552', '4112690', '4134588', '4105246', '4110619', '4122478', '4131491', '4105229', '4106874', '4104715', '4109031', '4121318', '4123523', '4127243', '4144573', '4104950', '4118321', '4134152', '4116334', '4127427', '4128067', '4097027', '4148821', '4109503', '4121143', '4123399', '4131972', '4138815', '4117663', '4110527', '4113290', '4125381', '4126249', '4126486', '4129075', '4102119', '4138119', '4144442', '4147898', '4134281', '4097524', '4109666', '4119472', '4124291', '4128399', '4138495', '4146542', '4116842', '4117193', '4134097', '4134142', '4136920', '4142569', '3793315', '3793720', '4117939', '4122116', '4126238', '4134467', '4139713', '4143482', '4106190', '4111666', '4113172', '4113848', '4111060', '4136352', '4143435', '4145385', '4106880', '4123459', '4119953', '4130813', '4142286', '4109046', '4111858', '3317897', '4111395', '4123494', '4124103', '4125050', '4142773', '4143441', '2770783', '2770760', '4105404', '4108804', '4110682', '4111065', '4112681', '4114896', '4116535', '4116839', '4117156', '4117657', '4117631', '4118546', '4119150', '4119604', '4121902', '4122330', '4122404', '4123305', '4123822', '4124851', '4126431', '4131947', '4133887', '4135215', '4138092', '4138549', '4138646', '4139622', '4141582', '4142154', '4142471', '4143899', '3277555', '3277536', '3277558', '3277551', '3790431', '3790975', '3792074', '3314025', '3314737', '3316876', '3317294', '3317114', '3317795', '3318515', '3318703', '3318695', '3317886', '3319131', '3319089', '3317548', '3794019', '3794664', '3765715', '3221825', '3222013', '3231279', '3231303', '4144733', '4101936', '4102354', '4104718', '4105378', '4105649', '4105992', '4106401', '4106671', '4106831', '4106938', '4107512', '4107530', '4108052', '4108651', '4108775', '4104879', '4109739', '4109850', '4110017', '4107910', '4111415', '4111459', '4112605', '4112719', '4112826', '4110879', '4107066', '4114016', '4113989', '4110361', '4110138', '4115087', '4115096', '4115763', '4108878', '4115883', '4116750', '4117140', '4117153', '4117196', '4117577', '4109058', '4104961', '4119542', '4119894', '4119126', '4121068', '4121181', '4121279', '4121920', '4114150', '4122661', '4122781', '4113156', '4123036', '4123491', '4123651', '4123770', '4124297', '4124580', '4124648', '4124795', '4124799', '4125309', '4125700', '4126397', '4127457', '4128030', '4128815', '4129529', '4123373', '4131139', '4131235', '4131610', '4132117', '4133146', '4133415', '4133805', '4126144', '4133963', '4134445', '4129606', '4132357', '4134704', '4134770', '4135152', '4135678', '4136251', '4136396', '4136424', '4136955', '4137000', '4103222', '4104433', '4094730', '4082695', '4103636', '4104445', '4092233', '4102878', '4103624', '4097460', '4100690', '4104254', '4138031', '4138095', '4139370', '4139811', '4141205', '4141546', '4141615', '4141881', '4142123', '4142151', '4143470', '4135853', '4143987', '4144519', '4113490', '4145364', '4145645', '4141096', '4146590', '4146607', '4141748', '4137898', '4148798', '4129346', '4115092', '4116754', '4124804', '4113040', '4133862', '4144278', '4148722')
							  	
						"]
ns_log Notice "invIDs $invoice_ids"
        set csv_contents [list]
        
         set csv_values [list "Kontonummer"]; # 0 Kontonummer
		lappend csv_values " Gegenkonto"; # 1 Gegenkonto
	     lappend csv_values "Bruttobetrag" ; # 2 Bruttobetrag
		lappend csv_values "Soll / Haben" ; # 3 Soll / Haben
		lappend csv_values "Belegnummer" ; # 4 Belegnummer
		lappend csv_values " Belegnummer 2" ; # 5 Belegnummer 2
		lappend csv_values "Steuerschlüssel" ; # 6 Steuerschlüssel
		lappend csv_values "Kostenstelle" ; # 7 Kostenstelle
		lappend csv_values "Kostenträger" ; # 8 Kostenträger
		lappend csv_values "Menge" ; # 9 Menge
		lappend csv_values "Belegdatum" ; # 10 Belegdatum
		lappend csv_values "Fähligkeitsdatum" ; # 11 Fähligkeitsdatum
		lappend csv_values "Buchungstext" ; # 12 Buchungstext
		lappend csv_values "Skontobetrag" ; # 13 Skontobetrag
		lappend csv_values "Steuerschlüssel Skonto" ; # 14 Steuerschlüssel Skonto
		lappend csv_values "Skontiziel" ; # 15 Skontiziel
		lappend csv_values "Skontierfähiger Betrag" ; # 16	 Skontierfähiger Betrag	
		lappend csv_values "Währung" ; # 17 Währung
		lappend csv_values "ID Nummer ZM Meldung" ; # 18 ID Nummer ZM Meldung
		lappend csv_values "Kontenart ((D)ebitor / (K)reditor / (S)achkonto)" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
		lappend csv_values "Suchnamen" ; # 20 Suchnamen
		lappend csv_values "Anrede" ; # 21 Anrede
		lappend csv_values "Vorname / Nachname" ; # 22 Vorname / Nachname
		lappend csv_values "Branche" ; # 23 Branche
		lappend csv_values "Strasse" ; # 24 Strasse
		lappend csv_values "Postleitzahl" ; # 25 Postleitzahl
		lappend csv_values "Ort" ; # 26 Ort
		lappend csv_values "Land" ; # 27 Land
		lappend csv_values "Kontonummer" ; # 28 Kontonummer
		lappend csv_values "Bankleitzahl" ; # 29 Bankleitzahl														
		lappend csv_values "Nettotage" ; # 30 Nettotage
		lappend csv_values "Skontotage" ; # 31 Skontotage
		lappend csv_values "Skontoprozent" ; # 32 Skontoprozent
		lappend csv_values "Telefonnummer" ; # 33 Telefonnummer
		lappend csv_values "Telefaxnummber" ; # 34 Telefaxnummber
		lappend csv_values "Sammelkonto" ; # 35 Sammelkonto
		lappend csv_values "Lastschrifteinzug" ; # 36 Lastschrifteinzug
		lappend csv_values "Selektion" ; # 37 Selektion
		lappend csv_values "Sachverhalt 13b" ; # 38 Sachverhalt 13b
		lappend csv_values "Dokument zur Erfassung" ; # 39 Dokument zur Erfassung
		lappend csv_values "IBAN" ; # 40 IBAN
		lappend csv_values "BIC" ; # 41 BIC
		lappend csv_values "Bankbezeichnung" ; # 42 Bankbezeichnung
		lappend csv_values "Vorname" ; # 43 Vorname
		lappend csv_values "Kundennummer" ; # 44 Kundennummer
		lappend csv_values "Freigabe Factoring" ; # 45 Freigabe Factoring
		lappend csv_values "Obligonummer" ; # 46	Obligonummer
		lappend csv_values "Mandatsreferenznummer" ; # 47 Mandatsreferenznummer
		lappend csv_values "Datum Mandatsreferenz" ; # 48 Datum Mandatsreferenz
		lappend csv_values "Limit" ; # 49 Limit
		lappend csv_values "Limit gültig ab" ; # 50 Limit gültig ab
		lappend csv_values "Konzern" ; # 51 Konzern
		lappend csv_values "del_Mod Datum" ; # 53 Mod Datum
		lappend csv_values "del_Creation Datum" ; # 53 Mod Datum
		lappend csv_values "del_invoice_id" ; # 52 invoice id
		lappend csv_values "del_today" ; # 53 Export Datum



		set csv_line [::csv::join $csv_values ";"]
		lappend csv_contents $csv_line





        foreach invoice_id $invoice_ids {
            
            set invpdf [etm_find_invoice_pdf -invoice_id $invoice_id]
            
            if { $invpdf ne "" } {
	      # HMD expects carriage return and linefeed
	      set content "[intranet_hmd::invoice_check_csv -invoice_id $invoice_id]"
	      if {$content eq ""} {
		  lappend failed_invoice_ids $invoice_id
	      } else {
		  lappend csv_contents $content
		  db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't', exported_to_hmd_date = :today where invoice_id = :invoice_id"
	      }
	    }
	}
        
        if {$csv_contents ne ""} {
            set csv_content [join $csv_contents "\r\n"]
            
            set fbasc_csv [ns_tmpnam]
            set file [open $fbasc_csv w]
            fconfigure $file -encoding "iso8859-1"
            puts $file $csv_content
            flush $file
            close $file
            set lastmonth [clock format [clock scan "1 month ago" -base [clock seconds]] -format "%Y%m"]
 			set cost_type "AR"
            if { $cost_type_id eq [im_cost_type_correction_invoice] } { set cost_type "AR-Korr" }
            file rename $fbasc_csv ${zip_dir}/${company_name}.$cost_type.${month}_orig.csv
            set file_created_p 1
        }
        
        
#         # ---------------------------------------------------------------
#         # Get the list of invoices created within a month and effective_date before than month
#         # ---------------------------------------------------------------
#         
# #         db_1row todays_date "
# # 		select
# # 			to_char(sysdate::date, 'YYYY') as todays_year,
# # 			to_char(sysdate::date, 'MM') as todays_month,
# # 			to_char(sysdate::date, 'DD') as todays_day
# # 		from dual
# # 		"
# 	
# #         set firstofmonth "$todays_year-$todays_month-01"
# #		set today [db_string get_today_date "select sysdate from dual"]
#         
#         set invoice_ids [db_list export_invoices "select c.cost_id 
# 										from im_costs c,
# 											im_projects p,
# 											im_invoices i,
# 											acs_objects acs
# 										where cost_type_id = :cost_type_id 
# 											and i.invoice_id = acs.object_id
# 											and p.project_id = c.project_id 
# 											and p.interco_company_id = :company_id 
# 											and i.invoice_id = c.cost_id 
# 											--and i.exported_to_hmd_p = 'f'
# 											--and effective_date >= date_trunc('month', current_date - interval '1' month)
# 											--and effective_date < date_trunc('month', current_date)
# 											and to_char(effective_date, 'YYYYMM') <  :next_month	
# 											and to_char(creation_date, 'YYYYMM') =  :month
# 
# 										"]
# 
#         set csv_contents [list]
#         
#          set csv_values [list "Kontonummer"]; # 0 Kontonummer
# 		lappend csv_values " Gegenkonto"; # 1 Gegenkonto
# 	     lappend csv_values "Bruttobetrag" ; # 2 Bruttobetrag
# 		lappend csv_values "Soll / Haben" ; # 3 Soll / Haben
# 		lappend csv_values "Belegnummer" ; # 4 Belegnummer
# 		lappend csv_values " Belegnummer 2" ; # 5 Belegnummer 2
# 		lappend csv_values "Steuerschlüssel" ; # 6 Steuerschlüssel
# 		lappend csv_values "Kostenstelle" ; # 7 Kostenstelle
# 		lappend csv_values "Kostenträger" ; # 8 Kostenträger
# 		lappend csv_values "Menge" ; # 9 Menge
# 		lappend csv_values "Belegdatum" ; # 10 Belegdatum
# 		lappend csv_values "Fähligkeitsdatum" ; # 11 Fähligkeitsdatum
# 		lappend csv_values "Buchungstext" ; # 12 Buchungstext
# 		lappend csv_values "Skontobetrag" ; # 13 Skontobetrag
# 		lappend csv_values "Steuerschlüssel Skonto" ; # 14 Steuerschlüssel Skonto
# 		lappend csv_values "Skontiziel" ; # 15 Skontiziel
# 		lappend csv_values "Skontierfähiger Betrag" ; # 16	 Skontierfähiger Betrag	
# 		lappend csv_values "Währung" ; # 17 Währung
# 		lappend csv_values "ID Nummer ZM Meldung" ; # 18 ID Nummer ZM Meldung
# 		lappend csv_values "Kontenart ((D)ebitor / (K)reditor / (S)achkonto)" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
# 		lappend csv_values "Suchnamen" ; # 20 Suchnamen
# 		lappend csv_values "Anrede" ; # 21 Anrede
# 		lappend csv_values "Vorname / Nachname" ; # 22 Vorname / Nachname
# 		lappend csv_values "Branche" ; # 23 Branche
# 		lappend csv_values "Strasse" ; # 24 Strasse
# 		lappend csv_values "Postleitzahl" ; # 25 Postleitzahl
# 		lappend csv_values "Ort" ; # 26 Ort
# 		lappend csv_values "Land" ; # 27 Land
# 		lappend csv_values "Kontonummer" ; # 28 Kontonummer
# 		lappend csv_values "Bankleitzahl" ; # 29 Bankleitzahl														
# 		lappend csv_values "Nettotage" ; # 30 Nettotage
# 		lappend csv_values "Skontotage" ; # 31 Skontotage
# 		lappend csv_values "Skontoprozent" ; # 32 Skontoprozent
# 		lappend csv_values "Telefonnummer" ; # 33 Telefonnummer
# 		lappend csv_values "Telefaxnummber" ; # 34 Telefaxnummber
# 		lappend csv_values "Sammelkonto" ; # 35 Sammelkonto
# 		lappend csv_values "Lastschrifteinzug" ; # 36 Lastschrifteinzug
# 		lappend csv_values "Selektion" ; # 37 Selektion
# 		lappend csv_values "Sachverhalt 13b" ; # 38 Sachverhalt 13b
# 		lappend csv_values "Dokument zur Erfassung" ; # 39 Dokument zur Erfassung
# 		lappend csv_values "IBAN" ; # 40 IBAN
# 		lappend csv_values "BIC" ; # 41 BIC
# 		lappend csv_values "Bankbezeichnung" ; # 42 Bankbezeichnung
# 		lappend csv_values "Vorname" ; # 43 Vorname
# 		lappend csv_values "Kundennummer" ; # 44 Kundennummer
# 		lappend csv_values "Freigabe Factoring" ; # 45 Freigabe Factoring
# 		lappend csv_values "Obligonummer" ; # 46	Obligonummer
# 		lappend csv_values "Mandatsreferenznummer" ; # 47 Mandatsreferenznummer
# 		lappend csv_values "Datum Mandatsreferenz" ; # 48 Datum Mandatsreferenz
# 		lappend csv_values "Limit" ; # 49 Limit
# 		lappend csv_values "Limit gültig ab" ; # 50 Limit gültig ab
# 		lappend csv_values "Konzern" ; # 51 Konzern
# 
# 
# 
# 		set csv_line [::csv::join $csv_values ";"]
#           lappend csv_contents $csv_line
# 
# 
# 
# 
# 
#         foreach invoice_id $invoice_ids {
#             
#             # HMD expects carriage return and linefeed
#             set content "[intranet_hmd::invoice_check_csv -invoice_id $invoice_id]"
#             if {$content eq ""} {
#                 lappend failed_invoice_ids $invoice_id
#             } else {
#                 lappend csv_contents $content
#                 #db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't', exported_to_hmd_date = :today where invoice_id = :invoice_id"
#             }
#         }
#         
#         if {$csv_contents ne ""} {
#             set csv_content [join $csv_contents "\r\n"]
#             
#             set fbasc_csv [ns_tmpnam]
#             set file [open $fbasc_csv w]
#             fconfigure $file -encoding "iso8859-1"
#             puts $file $csv_content
#             flush $file
#             close $file
#             set lastmonth [clock format [clock scan "1 month ago" -base [clock seconds]] -format "%Y%m"]
#  			set cost_type "AR"
#             if { $cost_type_id eq [im_cost_type_correction_invoice] } { set cost_type "AR-Korr" }
#             file rename $fbasc_csv ${zip_dir}/${company_name}.$cost_type.Eingang${month}_orig.csv
#             set file_created_p 1
#         }
    }   
    
}

# ---------------------------------------------------------------
# Provider bills
# ---------------------------------------------------------------

set invoice_ids [db_list export_invoices  "
			select c.cost_id 
			from im_costs c,im_invoices i 
			where cost_type_id = [im_cost_type_bill] 
				 and i.invoice_id = c.cost_id 
				 and i.exported_to_hmd_p <> 't'
				 and effective_date < date_trunc('month', current_date)
				 "]
    
set csv_contents [list]


		set csv_values [list "Kontonummer"]; # 0 Kontonummer
		lappend csv_values " Gegenkonto"; # 1 Gegenkonto
		lappend csv_values "Bruttobetrag" ; # 2 Bruttobetrag
		lappend csv_values "Soll / Haben" ; # 3 Soll / Haben
		lappend csv_values "Belegnummer" ; # 4 Belegnummer
		lappend csv_values " Belegnummer 2" ; # 5 Belegnummer 2
		lappend csv_values "Steuerschlüssel" ; # 6 Steuerschlüssel
		lappend csv_values "Kostenstelle" ; # 7 Kostenstelle
		lappend csv_values "Kostenträger" ; # 8 Kostenträger
		lappend csv_values "Menge" ; # 9 Menge
		lappend csv_values "Belegdatum" ; # 10 Belegdatum
		lappend csv_values "Fähligkeitsdatum" ; # 11 Fähligkeitsdatum
		lappend csv_values "Buchungstext" ; # 12 Buchungstext
		lappend csv_values "Skontobetrag" ; # 13 Skontobetrag
		lappend csv_values "Steuerschlüssel Skonto" ; # 14 Steuerschlüssel Skonto
		lappend csv_values "Skontiziel" ; # 15 Skontiziel
		lappend csv_values "Skontierfähiger Betrag" ; # 16	 Skontierfähiger Betrag	
		lappend csv_values "Währung" ; # 17 Währung
		lappend csv_values "ID Nummer ZM Meldung" ; # 18 ID Nummer ZM Meldung
		lappend csv_values "Kontenart ((D)ebitor / (K)reditor / (S)achkonto)" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
		lappend csv_values "Suchnamen" ; # 20 Suchnamen
		lappend csv_values "Anrede" ; # 21 Anrede
		lappend csv_values "Vorname / Nachname" ; # 22 Vorname / Nachname
		lappend csv_values "Branche" ; # 23 Branche
		lappend csv_values "Strasse" ; # 24 Strasse
		lappend csv_values "Postleitzahl" ; # 25 Postleitzahl
		lappend csv_values "Ort" ; # 26 Ort
		lappend csv_values "Land" ; # 27 Land
		lappend csv_values "Kontonummer" ; # 28 Kontonummer
		lappend csv_values "Bankleitzahl" ; # 29 Bankleitzahl														
		lappend csv_values "Nettotage" ; # 30 Nettotage
		lappend csv_values "Skontotage" ; # 31 Skontotage
		lappend csv_values "Skontoprozent" ; # 32 Skontoprozent
		lappend csv_values "Telefonnummer" ; # 33 Telefonnummer
		lappend csv_values "Telefaxnummber" ; # 34 Telefaxnummber
		lappend csv_values "Sammelkonto" ; # 35 Sammelkonto
		lappend csv_values "Lastschrifteinzug" ; # 36 Lastschrifteinzug
		lappend csv_values "Selektion" ; # 37 Selektion
		lappend csv_values "Sachverhalt 13b" ; # 38 Sachverhalt 13b
		lappend csv_values "Dokument zur Erfassung" ; # 39 Dokument zur Erfassung
		lappend csv_values "IBAN" ; # 40 IBAN
		lappend csv_values "BIC" ; # 41 BIC
		lappend csv_values "Bankbezeichnung" ; # 42 Bankbezeichnung
		lappend csv_values "Vorname" ; # 43 Vorname
		lappend csv_values "Kundennummer" ; # 44 Kundennummer
		lappend csv_values "Freigabe Factoring" ; # 45 Freigabe Factoring
		lappend csv_values "Obligonummer" ; # 46	Obligonummer
		lappend csv_values "Mandatsreferenznummer" ; # 47 Mandatsreferenznummer
		lappend csv_values "Datum Mandatsreferenz" ; # 48 Datum Mandatsreferenz
		lappend csv_values "Limit" ; # 49 Limit
		lappend csv_values "Limit gültig ab" ; # 50 Limit gültig ab
		lappend csv_values "Konzern" ; # 51 Konzern
		lappend csv_values "del_Mod Datum" ; # 53 Mod Datum
		lappend csv_values "del_Creation Datum" ; # 53 Mod Datum
		lappend csv_values "del_invoice_id" ; # 52 invoice id
		lappend csv_values "del_today" ; # 53 Export Datum

set csv_line [::csv::join $csv_values ";"]
lappend csv_contents $csv_line


foreach invoice_id $invoice_ids {

	set today [db_string get_today_date "select sysdate from dual"]
    # HMD expects carriage return and linefeed
    set content "[intranet_hmd::bill_check_csv -invoice_id $invoice_id]"
    if {$content eq ""} {
        lappend failed_invoice_ids $invoice_id
    } else {
        lappend csv_contents $content
        db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't', exported_to_hmd_date = :today where invoice_id = :invoice_id"
    }
}
    
if {$csv_contents ne ""} {
    set csv_content [join $csv_contents "\r\n"]
    
    set fbasc_csv [ns_tmpnam]
    set file [open $fbasc_csv w]
    fconfigure $file -encoding "iso8859-1"
    puts $file $csv_content
    flush $file
    close $file
    set lastmonth [clock format [clock scan "1 month ago" -base [clock seconds]] -format "%Y%m"]
    file rename $fbasc_csv ${zip_dir}/FUD.ER.fbasc.${lastmonth}_orig.csv
    set file_created_p 1
}


if {$file_created_p} {
    # Generate the zipfile
    set zip_file [ns_tmpnam]
    exec zip -r $zip_file ${zip_dir}
    
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"FBASC_$lastmonth.csv.zip\""
    ns_returnfile 200 application/zip ${zip_file}.zip
    if {$failed_invoice_ids eq ""} {set failed_invoice_ids "none"}
    acs_mail_lite::send -to_addr [ad_system_owner] -from_addr [ad_system_owner] -subject "Report HMD Export" -body "Failed Invoices: $failed_invoice_ids "
    
    file delete -force ${zip_file}.zip
    file delete -force $zip_dir
} else {
    ad_return_error "No new bookings" "No file generated, no new invoices found"
}