# /package/intranet-hmd/download-invoices.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Download the HMD FBASC CSV File.

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @creation-date November 2016
} {
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]
set page_title "Download FBASC"

set current_date [ns_localsqltimestamp]


# ---------------------------------------------------------------
# Provider bills
# ---------------------------------------------------------------

# set invoice_ids [db_list export_invoices "select c.cost_id from im_costs c,im_invoices i 
#     where cost_type_id in ([template::util::tcl_to_sql_list [list [im_cost_type_bill] [im_cost_type_correction_bill]]]) 
#     and i.invoice_id = c.cost_id 
#     and i.exported_to_hmd_p = 'f'
# 
# "]


set invoice_ids [db_list export_invoices "
select c.cost_id from im_costs c,im_invoices i , acs_objects a
    where cost_type_id = [im_cost_type_bill]
    --in ([template::util::tcl_to_sql_list [list [im_cost_type_bill] [im_cost_type_correction_bill]]]) 
    and i.invoice_id = c.cost_id 
    --and i.exported_to_hmd_p = 'f'
    and cost_id = object_id 
    and to_char(a.last_modified, 'YYYY') = '2018'
"]



 set csv_contents [list]
      
        set csv_values [list "Kontonummer"]; # 0 Kontonummer
        lappend csv_values " Gegenkonto"; # 1 Gegenkonto
        lappend csv_values "Bruttobetrag" ; # 2 Bruttobetrag
        lappend csv_values "Soll / Haben" ; # 3 Soll / Haben
        lappend csv_values "Belegnummer" ; # 4 Belegnummer
        lappend csv_values " Belegnummer 2" ; # 5 Belegnummer 2
        lappend csv_values "Steuerschlüssel" ; # 6 Steuerschlüssel
        lappend csv_values "Kostenstelle" ; # 7 Kostenstelle
        lappend csv_values "Kostenträger" ; # 8 Kostenträger
        lappend csv_values "Menge" ; # 9 Menge
        lappend csv_values "Belegdatum" ; # 10 Belegdatum
        lappend csv_values "Fähligkeitsdatum" ; # 11 Fähligkeitsdatum
        lappend csv_values "Buchungstext" ; # 12 Buchungstext
        lappend csv_values "Skontobetrag" ; # 13 Skontobetrag
        lappend csv_values "Steuerschlüssel Skonto" ; # 14 Steuerschlüssel Skonto
        lappend csv_values "Skontiziel" ; # 15 Skontiziel
        lappend csv_values "Skontierfähiger Betrag" ; # 16   Skontierfähiger Betrag 
        lappend csv_values "Währung" ; # 17 Währung
        lappend csv_values "ID Nummer ZM Meldung" ; # 18 ID Nummer ZM Meldung
        lappend csv_values "Kontenart ((D)ebitor / (K)reditor / (S)achkonto)" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
        lappend csv_values "Suchnamen" ; # 20 Suchnamen
        lappend csv_values "Anrede" ; # 21 Anrede
        lappend csv_values "Vorname / Nachname" ; # 22 Vorname / Nachname
        lappend csv_values "Branche" ; # 23 Branche
        lappend csv_values "Strasse" ; # 24 Strasse
        lappend csv_values "Postleitzahl" ; # 25 Postleitzahl
        lappend csv_values "Ort" ; # 26 Ort
        lappend csv_values "Land" ; # 27 Land
        lappend csv_values "Kontonummer" ; # 28 Kontonummer
        lappend csv_values "Bankleitzahl" ; # 29 Bankleitzahl                                                       
        lappend csv_values "Nettotage" ; # 30 Nettotage
        lappend csv_values "Skontotage" ; # 31 Skontotage
        lappend csv_values "Skontoprozent" ; # 32 Skontoprozent
        lappend csv_values "Telefonnummer" ; # 33 Telefonnummer
        lappend csv_values "Telefaxnummber" ; # 34 Telefaxnummber
        lappend csv_values "Sammelkonto" ; # 35 Sammelkonto
        lappend csv_values "Lastschrifteinzug" ; # 36 Lastschrifteinzug
        lappend csv_values "Selektion" ; # 37 Selektion
        lappend csv_values "Sachverhalt 13b" ; # 38 Sachverhalt 13b
        lappend csv_values "Dokument zur Erfassung" ; # 39 Dokument zur Erfassung
        lappend csv_values "IBAN" ; # 40 IBAN
        lappend csv_values "BIC" ; # 41 BIC
        lappend csv_values "Bankbezeichnung" ; # 42 Bankbezeichnung
        lappend csv_values "Vorname" ; # 43 Vorname
        lappend csv_values "Kundennummer" ; # 44 Kundennummer
        lappend csv_values "Freigabe Factoring" ; # 45 Freigabe Factoring
        lappend csv_values "Obligonummer" ; # 46    Obligonummer
        lappend csv_values "Mandatsreferenznummer" ; # 47 Mandatsreferenznummer
        lappend csv_values "Datum Mandatsreferenz" ; # 48 Datum Mandatsreferenz
        lappend csv_values "Limit" ; # 49 Limit
        lappend csv_values "Limit gültig ab" ; # 50 Limit gültig ab
        lappend csv_values "Konzern" ; # 51 Konzern
    
        set csv_line [::csv::join $csv_values ";"]
        lappend csv_contents $csv_line

set failed_invoice_ids [list]
foreach invoice_id $invoice_ids {

    # HMD expects carriage return and linefeed
    set content "[intranet_hmd::bill_check_csv -invoice_id $invoice_id]"
    if {$content eq ""} {
        lappend failed_invoice_ids $invoice_id
    } else {
        lappend csv_contents $content
        #db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't' where invoice_id = :invoice_id"
        #db_dml exported_date "update im_invoices set exported_to_hmd_date = :current_date where invoice_id=:invoice_id"

    }
}

set csv_content [join $csv_contents "\r\n"]

set fbasc_csv [ns_tmpnam]
set file [open $fbasc_csv w]
fconfigure $file -encoding "iso8859-1"
puts $file $csv_content
flush $file
close $file


# Return the file
set outputheaders [ns_conn outputheaders]
ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"Provider.fbasc.hia\""
ns_returnfile 200 text/text $fbasc_csv
acs_mail_lite::send -to_addr malte.sussdorff@cognovis.de -subject "HMD Export report" "Failed Downloads: $failed_invoice_ids "

file delete -force $fbasc_csv

