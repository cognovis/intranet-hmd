# packages/intranet-hmd/tcl/intranet-hmd-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {

	Procedure to interact with hmd

	@author <yourname> (<your email>)
	@creation-date 2012-01-04
	@cvs-id $Id$
}

namespace eval intranet_hmd {}
package require csv

ad_proc -public intranet_hmd::invoice_csv {
	-invoice_id
} {
	Generate a CSV Line for this invoice in FBASC format

	@param invoice_id Invoice to be sent over to HMD
} {
	
	# Get all the invoice information
	if {![db_0or1row invoice_data {
		select to_char(effective_date,'DD.MM.YYYY') as invoice_date, effective_date, invoice_nr, cost_type_id, ci.vat_type_id as invoice_vat_type_id, c.vat_type_id as company_vat_type_id, i.company_contact_id, im_name_from_id(i.company_contact_id) as company_contact,
		  round(coalesce(vat_amount,0),2) as vat_amount, round(coalesce(amount,0),2) as amount, c.company_id, c.vat_number, c.company_path, c.company_name, c.company_type_id,
		  to_char(ci.effective_date::date + ci.payment_days,'DD.MM.YYYY') AS due_date,
		  ci.payment_days,
		  cc.cost_center_code as kostenstelle,
		  o.address_line1, o.address_city, o.address_postal_code, upper(o.address_country_code) as country_code,ci.currency, round(((ci.amount + ci.vat_amount) *
im_exchange_rate(ci.effective_date::date, ci.currency, 'EUR')) :: numeric
, 2) as buchungsbetrag_eur
		from im_invoices i, im_costs ci, im_companies c, im_offices o, im_cost_centers cc
		where c.company_id = ci.customer_id 
			and c.main_office_id = o.office_id
			and ci.cost_id = i.invoice_id 
			and cc.cost_center_id = ci.cost_center_id
			and i.invoice_id = :invoice_id
	}]} {
		ns_log Error "Could not get information for invoice_id $invoice_id"
		return ""
		ad_script_abort
	} else {

		set total_amount [lc_numeric [expr $vat_amount + $amount] "%.2f" "de_DE"]
		
	    if {$invoice_vat_type_id eq ""} {
			set vat_type_id $company_vat_type_id
	    } else {
			set vat_type_id $invoice_vat_type_id
	    }
	
	    if {![db_0or1row vat_data "
			select ca.aux_int1 as customer_vat, ca.aux_int2 as customer_konto
			from im_categories ca
			where ca.category_id = :vat_type_id"]} {
				set customer_vat ""
				set customer_konto ""
		}
				
	
		ns_log Notice "Generating HMD invoice CSV line for $invoice_id without line items"
		
		# ---------------------------------------------------------------
		# Search name as per FUD convention
		# ---------------------------------------------------------------

		switch $company_type_id {
			11000010 {
				# Private Customer
				if {$company_contact eq $company_name} {
					set username [db_string username "select username from users where user_id = :company_contact_id" -default "company_name"]
					set search_name "[string range $company_contact 0 2]"
				} else {
					set last_word [lindex [split $company_name " "] end]
					set search_name [string range $last_word 0 2]
				}
			}
			11000011 {
				# Business Customer - 3 erste buchstaben des Firmennamens
				set search_name "[string range $company_name 0 2]"
			}
			default {
				set search_name "[string range $company_path 0 14]"
			}
		}
		
		# ---------------------------------------------------------------
		# Währung nur in EUR
		# ---------------------------------------------------------------

		set buchungstext "Beleg $invoice_nr"
		
		if {$currency ne "EUR"} {
			# 3. alle Betraege in EUR, und in Feld "12 Buchungstext" noch den Originalbetrag anhängen
			#######
			append buchungstext " ($total_amount $currency)"
			set total_amount $buchungsbetrag_eur
		}

		set csv_values [list $company_id]; # 0 Kontonummer
		lappend csv_values "$customer_konto"; # 1 Gegenkonto
	    lappend csv_values "$total_amount" ; # 2 Bruttobetrag
		lappend csv_values "S" ; # 3 Soll / Haben
		lappend csv_values "$invoice_nr" ; # 4 Belegnummer
		lappend csv_values "" ; # 5 Belegnummer 2
		lappend csv_values "" ; # 6 Steuerschlüssel
		lappend csv_values "" ; # 7 Kostenstelle
		lappend csv_values "" ; # 8 Kostenträger
		lappend csv_values "" ; # 9 Menge
		lappend csv_values "$invoice_date" ; # 10 Belegdatum
		lappend csv_values "$due_date" ; # 11 Fähligkeitsdatum
		lappend csv_values "$buchungstext" ; # 12 Buchungstext
		lappend csv_values "" ; # 13 Skontobetrag
		lappend csv_values "" ; # 14 Steuerschlüssel Skonto
		lappend csv_values "" ; # 15 Skontiziel
		lappend csv_values "" ; # 16	 Skontierfähiger Betrag	
		lappend csv_values "" ; # 17 Währung
		lappend csv_values "$vat_number" ; # 18 ID Nummer ZM Meldung
		lappend csv_values "D" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
		lappend csv_values "$search_name" ; # 20 Suchnamen
		lappend csv_values "" ; # 21 Anrede
		lappend csv_values "$company_name" ; # 22 Vorname / Nachname
		lappend csv_values "" ; # 23 Branche
		lappend csv_values "$address_line1" ; # 24 Strasse
		lappend csv_values "$address_postal_code" ; # 25 Postleitzahl
		lappend csv_values "$address_city" ; # 26 Ort
		lappend csv_values "$country_code" ; # 27 Land
		lappend csv_values "" ; # 28 Kontonummer
		lappend csv_values "" ; # 29 Bankleitzahl														
		lappend csv_values "$payment_days" ; # 30 Nettotage
		lappend csv_values "" ; # 31 Skontotage
		lappend csv_values "" ; # 32 Skontoprozent
		lappend csv_values "" ; # 33 Telefonnummer
		lappend csv_values "" ; # 34 Telefaxnummber
		lappend csv_values "" ; # 35 Sammelkonto
		lappend csv_values "" ; # 36 Lastschrifteinzug
		lappend csv_values "" ; # 37 Selektion
		lappend csv_values "" ; # 38 Sachverhalt 13b
		lappend csv_values "" ; # 39 Dokument zur Erfassung
		lappend csv_values "" ; # 40 IBAN
		lappend csv_values "" ; # 41 BIC
		lappend csv_values "" ; # 42 Bankbezeichnung
		lappend csv_values "" ; # 43 Vorname
		lappend csv_values "$company_id" ; # 44 Kundennummer
		lappend csv_values "" ; # 45 Freigabe Factoring
		lappend csv_values "" ; # 46	Obligonummer
		lappend csv_values "" ; # 47 Mandatsreferenznummer
		lappend csv_values "" ; # 48 Datum Mandatsreferenz
		lappend csv_values "" ; # 49 Limit
		lappend csv_values "" ; # 50 Limit gültig ab
		lappend csv_values "" ; # 51 Konzern
	
		set csv_line [::csv::join $csv_values ";"]
	
		return "$csv_line"
	}
}

ad_proc -public intranet_hmd::bill_csv {
	-invoice_id
} {
	Generate a CSV Line for this provider bill in FBASC format

	@param invoice_id Invoice to be sent over to HMD
} {
	set corr_invoice_nr ""

	# Get all the invoice information
	if {![db_0or1row invoice_data {
		select to_char(effective_date,'DD.MM.YYYY') as invoice_date, effective_date, invoice_nr, cost_type_id, ci.vat_type_id as invoice_vat_type_id, c.vat_type_id as company_vat_type_id, i.company_contact_id, im_name_from_id(i.company_contact_id) as company_contact,
		  round(coalesce(vat_amount,0),2) as vat_amount, round(coalesce(amount,0),2) as amount, c.company_id, c.vat_number, c.company_path, c.company_name, c.company_type_id,
		  to_char(ci.effective_date::date + ci.payment_days,'DD.MM.YYYY') AS due_date,
		  ci.payment_days,
		  cc.cost_center_code as kostenstelle,
		  o.address_line1, o.address_city, o.address_postal_code, upper(o.address_country_code) as country_code,ci.currency, round(((ci.amount + ci.vat_amount) *
im_exchange_rate(ci.effective_date::date, ci.currency, 'EUR')) :: numeric
, 2) as buchungsbetrag_eur
		from im_invoices i, im_costs ci, im_companies c, im_offices o, im_cost_centers cc
		where c.company_id = ci.provider_id 
			and c.main_office_id = o.office_id
			and ci.cost_id = i.invoice_id 
			and cc.cost_center_id = ci.cost_center_id
			and i.invoice_id = :invoice_id
	}]} {
		ns_log Error "Could not get information for invoice_id $invoice_id"
		return ""
	} else {

		set total_amount [lc_numeric [expr $vat_amount + $amount] "%.2f" "de_DE"]

		if {$invoice_vat_type_id eq ""} {
			set vat_type_id $company_vat_type_id
		} else {
			set vat_type_id $invoice_vat_type_id
		}

		if {![db_0or1row vat_data "
			select ca.aux_int1 as customer_vat, ca.aux_int2 as customer_konto
			from im_categories ca
			where ca.category_id = :vat_type_id"]} {
				set customer_vat ""
				set customer_konto ""
		}


		ns_log Notice "Generating HMD provider bill CSV line for $invoice_id without line items"

		# ---------------------------------------------------------------
		# Search name as per FUD convention
		# ---------------------------------------------------------------

		# Private Customer
		if {$company_contact eq $company_name} {
			set username [db_string username "select username from users where user_id = :company_contact_id" -default "company_name"]
			set search_name "[string range $company_contact 0 2]"
		} else {
			set last_word [lindex [split $company_name " "] end]
			set search_name [string range $last_word 0 2]
		}

		# ---------------------------------------------------------------
		# Währung nur in EUR
		# ---------------------------------------------------------------

		set buchungstext "Beleg $invoice_nr"

		if {$currency ne "EUR"} {
			# 3. alle Betraege in EUR, und in Feld "12 Buchungstext" noch den Originalbetrag anhängen
			#######
			append buchungstext " ($total_amount $currency)"
			set total_amount $buchungsbetrag_eur
		}

		set csv_values [list $company_id]; # 0 Kontonummer
		lappend csv_values "$customer_konto"; # 1 Gegenkonto
		lappend csv_values "$total_amount" ; # 2 Bruttobetrag
		lappend csv_values "S" ; # 3 Soll / Haben
		lappend csv_values "$invoice_nr" ; # 4 Belegnummer
		lappend csv_values "" ; # 5 Belegnummer 2
		lappend csv_values "" ; # 6 Steuerschlüssel
		lappend csv_values "" ; # 7 Kostenstelle
		lappend csv_values "" ; # 8 Kostenträger
		lappend csv_values "" ; # 9 Menge
		lappend csv_values "$invoice_date" ; # 10 Belegdatum
		lappend csv_values "$due_date" ; # 11 Fähligkeitsdatum
		lappend csv_values "$buchungstext" ; # 12 Buchungstext
		lappend csv_values "" ; # 13 Skontobetrag
		lappend csv_values "" ; # 14 Steuerschlüssel Skonto
		lappend csv_values "" ; # 15 Skontiziel
		lappend csv_values "" ; # 16	 Skontierfähiger Betrag	
		lappend csv_values "" ; # 17 Währung
		lappend csv_values "$vat_number" ; # 18 ID Nummer ZM Meldung
		lappend csv_values "K" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
		lappend csv_values "$search_name" ; # 20 Suchnamen
		lappend csv_values "" ; # 21 Anrede
		lappend csv_values "$company_name" ; # 22 Vorname / Nachname
		lappend csv_values "" ; # 23 Branche
		lappend csv_values "$address_line1" ; # 24 Strasse
		lappend csv_values "$address_postal_code" ; # 25 Postleitzahl
		lappend csv_values "$address_city" ; # 26 Ort
		lappend csv_values "$country_code" ; # 27 Land
		lappend csv_values "" ; # 28 Kontonummer
		lappend csv_values "" ; # 29 Bankleitzahl														
		lappend csv_values "$payment_days" ; # 30 Nettotage
		lappend csv_values "" ; # 31 Skontotage
		lappend csv_values "" ; # 32 Skontoprozent
		lappend csv_values "" ; # 33 Telefonnummer
		lappend csv_values "" ; # 34 Telefaxnummber
		lappend csv_values "" ; # 35 Sammelkonto
		lappend csv_values "" ; # 36 Lastschrifteinzug
		lappend csv_values "" ; # 37 Selektion
		lappend csv_values "" ; # 38 Sachverhalt 13b
		lappend csv_values "" ; # 39 Dokument zur Erfassung
		lappend csv_values "" ; # 40 IBAN
		lappend csv_values "" ; # 41 BIC
		lappend csv_values "" ; # 42 Bankbezeichnung
		lappend csv_values "" ; # 43 Vorname
		lappend csv_values "$company_id" ; # 44 Kundennummer
		lappend csv_values "" ; # 45 Freigabe Factoring
		lappend csv_values "" ; # 46	Obligonummer
		lappend csv_values "" ; # 47 Mandatsreferenznummer
		lappend csv_values "" ; # 48 Datum Mandatsreferenz
		lappend csv_values "" ; # 49 Limit
		lappend csv_values "" ; # 50 Limit gültig ab
		lappend csv_values "" ; # 51 Konzern

		set csv_line [::csv::join $csv_values ";"]

		return "$csv_line"
	}
}
